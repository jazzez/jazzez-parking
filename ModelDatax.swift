//
//  ModelData.swift
//  045 - InfoCard
//
//  Created by Mark Moeykens on 3/6/17.
//  Copyright © 2017 Moeykens. All rights reserved.
//

import UIKit

class ModelDatax {
    var namex = ""
    var infox = ""
    var imagex: UIImage?
    
    init(namex: String, infox: String, imagex: UIImage?) {
        self.namex = namex
        self.infox = infox
        self.imagex = imagex
    }
}
