//
//  CharacterCell.swift
//  045 - InfoCard
//
//  Created by Mark Moeykens on 3/6/17.
//  Copyright © 2017 Moeykens. All rights reserved.
//

import UIKit

class CharacterCellx: UITableViewCell {

    @IBOutlet weak var characterImagex: UIImageView!
    @IBOutlet weak var namex: UILabel!
    @IBOutlet weak var descx: UILabel!
    
    func setupx(itemx: ModelDatax) {
        namex.text = itemx.namex
        descx.text = itemx.infox
        characterImagex.image = itemx.imagex
    }
}
