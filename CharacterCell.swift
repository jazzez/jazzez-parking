//
//  CharacterCell.swift
//  045 - InfoCard
//
//  Created by Mark Moeykens on 3/6/17.
//  Copyright © 2017 Moeykens. All rights reserved.
//

import UIKit

class CharacterCell: UITableViewCell {

    @IBOutlet weak var characterImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var desc: UILabel!
    
    func setup(item: ModelData) {
        name.text = item.name
        desc.text = item.info
        characterImage.image = item.image
    }
}
