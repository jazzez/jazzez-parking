//
//  Data.swift
//  045 - InfoCard (TableView)
//
//  Created by Mark Moeykens on 3/12/17.
//  Copyright © 2017 Mark Moeykens. All rights reserved.
//

import UIKit

class Data {
    
    static func getData(completion: @escaping ([ModelData]) -> ()) {
        DispatchQueue.global(qos: .userInteractive).async {
        
            var data: [ModelData] = []
            
            let info = "Permitted parking only zones"
            
            data.append(ModelData(name: "Contractor", info: info, image: #imageLiteral(resourceName: "Contractor")))
            data.append(ModelData(name: "River", info: info, image: #imageLiteral(resourceName: "River")))
            data.append(ModelData(name: "Tri-Care", info: info, image: #imageLiteral(resourceName: "Tri-Care")))
            data.append(ModelData(name: "Distinguished Visitor", info: info, image: #imageLiteral(resourceName: "Distinguished Visitor")Distinguished Visitor))
            data.append(ModelData(name: "Handicap", info: info, image: #imageLiteral(resourceName: "Handicap")))
            data.append(ModelData(name: "Mall", info: info, image: #imageLiteral(resourceName: "Mall")))
            data.append(ModelData(name: "MC", info: info, image: #imageLiteral(resourceName: "MC")))
            data.append(ModelData(name: "North A", info: info, image: #imageLiteral(resourceName: "North A")))
            data.append(ModelData(name: "North C", info: info, image: #imageLiteral(resourceName: "North C")))
            data.append(ModelData(name: "North Secured", info: info, image: #imageLiteral(resourceName: "North Secured")))
            data.append(ModelData(name: "Official Business", info: info, image: #imageLiteral(resourceName: "Official Business")))
            data.append(ModelData(name: "Pentagon Conference Center", info: info, image: #imageLiteral(resourceName: "Pentagon Conference Center")))
            data.append(ModelData(name: "South C", info: info, image: #imageLiteral(resourceName: "South C")))
            data.append(ModelData(name: "South A", info: info, image: #imageLiteral(resourceName: "South A")))
            data.append(ModelData(name: "South A Secured", info: info, image: #imageLiteral(resourceName: "South A Secured")))
            data.append(ModelData(name: "Car Pool", info: info, image: #imageLiteral(resourceName: "Car Pool")))
            data.append(ModelData(name: "Corridor 5", info: info, image: #imageLiteral(resourceName: "Corridor 5")))
            data.append(ModelData(name: "Press", info: info, image: #imageLiteral(resourceName: "Press")))
            data.append(ModelData(name: "VIP", info: info, image: #imageLiteral(resourceName: "VIP")))
            
            
           sleep(2)
            
            DispatchQueue.main.async {
                completion(data)
            }
        }
    }
    
    //    func completion(data: [ModelData]) {
    //
        }
//}























