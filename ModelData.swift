//
//  ModelData.swift
//  045 - InfoCard
//
//  Created by Mark Moeykens on 3/6/17.
//  Copyright © 2017 Moeykens. All rights reserved.
//

import UIKit

class ModelData {
    var name = ""
    var info = ""
    var image: UIImage?
    
    init(name: String, info: String, image: UIImage?) {
        self.name = name
        self.info = info
        self.image = image
    }
}
