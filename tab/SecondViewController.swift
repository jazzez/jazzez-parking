//
//  SecondViewController.swift
//  tab
//
//  Created by Cole GOODIER on 8/31/17.
//  Copyright © 2017 Cole Goodier Jasint. All rights reserved.
//
import UIKit

class ViewControllerx: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var tableViewx: UITableView!
    @IBOutlet weak var dimViewx: UIView!
    
    
    //MARK: - INFO SCREEN
    @IBOutlet var infoViewx: UIView!
    @IBOutlet weak var infoNameLabelx: UILabel!
    @IBOutlet weak var infoImagex: UIImageView!
    
    var tableDatax: [ModelDatax] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let startx = Date()
        
        DispatchQueue.global(qos: .userInteractive).async {
            Datax.getDatax(completionx: { (datax) in
                self.tableDatax = datax
                DispatchQueue.main.async {
                    self.tableViewx.reloadData()
                }
                
            })
            
        }
        tableViewx.dataSource = self
        tableViewx.delegate = self
        
        let endx = Date()
        print("Elapsed Time: \(endx.timeIntervalSince(startx))")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableDatax.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
        -> UITableViewCell {
            let cellx = tableViewx.dequeueReusableCell(withIdentifier: "cellx") as! CharacterCellx
            cellx.setupx(itemx: tableDatax[indexPath.row])
            return cellx
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let datax = tableDatax[indexPath.row]
        
        infoImagex.image = datax.imagex
        infoNameLabelx.text = datax.namex
        infoViewx.center = view.center
        infoViewx.alpha = 1
        
        infoViewx.transform = CGAffineTransform(scaleX: 0.8, y: 1.2)
        
        view.addSubview(infoViewx)
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: {
            self.dimViewx.alpha = 0.8
            self.infoViewx.transform = .identity
        })
    }
    
    @IBAction func closeInfoPopupx(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: {
            self.dimViewx.alpha = 0
            self.infoViewx.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        }) { (success) in
            self.infoViewx.removeFromSuperview()
        }
    }
}
