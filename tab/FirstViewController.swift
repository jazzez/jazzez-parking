//
//  FirstViewController.swift
//  tab
//
//  Created by Cole GOODIER on 8/31/17.
//  Copyright © 2017 Cole Goodier Jasint. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dimView: UIView!
    
    
    //MARK: - INFO SCREEN
    @IBOutlet var infoView: UIView!
    @IBOutlet weak var infoNameLabel: UILabel!
    @IBOutlet weak var infoImage: UIImageView!
    
    var tableData: [ModelData] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let start = Date()
        
        DispatchQueue.global(qos: .userInteractive).async {
            Data.getData(completion: { (data) in
                self.tableData = data
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                
            })
        }
        
        tableView.dataSource = self
        tableView.delegate = self
        
        let end = Date()
        print("Elapsed Time: \(end.timeIntervalSince(start))")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
        -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CharacterCell
            cell.setup(item: tableData[indexPath.row])
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = tableData[indexPath.row]
        
        infoImage.image = data.image
        infoNameLabel.text = data.name
        infoView.center = view.center
        infoView.alpha = 1
        
        infoView.transform = CGAffineTransform(scaleX: 0.8, y: 1.2)
        
        view.addSubview(infoView)
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: {
            self.dimView.alpha = 0.8
            self.infoView.transform = .identity
        })
    }
    
    @IBAction func closeInfoPopup(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [], animations: {
            self.dimView.alpha = 0
            self.infoView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        }) { (success) in
            self.infoView.removeFromSuperview()
        }
    }
}


